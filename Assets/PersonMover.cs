using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class PersonMover : MonoBehaviour
{
    float directionChangeCooldown = 0.0f;
    const float DCCLIMIT = 5.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (directionChangeCooldown >= DCCLIMIT) {
            directionChangeCooldown = 0.0f;
            transform.rotation = Quaternion.Euler(new Vector3(0, new Random().Next(0, 360), 0));
        }

        directionChangeCooldown += Time.deltaTime;

        transform.Translate(0, 0, 0.5f * Time.deltaTime);
    }
}
