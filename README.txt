Changes Accord in All Sounds

Title Music: 
Casa Bossa Nova by Kevin MacLeod | https://incompetech.com/
Music promoted by https://www.chosic.com/free-music/all/
Creative Commons Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/ 
 
Game Music:
Wasteland Showdown [Battle Music] by Matthew Pablo | https://opengameart.org/content/wasteland-showdown-battle-music 
Music promoted by https://opengameart.org/ 
Creative Commons: https://creativecommons.org/licenses/by/3.0/ 

Monster Sound Effects:
15 Monster Grunt/Pain/Death Sounds by Michel Baradari | https://opengameart.org/content/15-monster-gruntpaindeath-sounds
Sound promoted by https://opengameart.org/ 
Creative Commons: https://creativecommons.org/licenses/by/3.0/ 

Punch by qubodup | https://opengameart.org/content/punch
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Environmental Sound Effects:
Ocean splash by Timras | https://opengameart.org/content/ocean-splash 
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Sea And River Wave Sounds by RandomMind | https://opengameart.org/content/sea-and-river-wave-sounds
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

5 break, crunch impacts by Independent.nu | https://opengameart.org/content/5-break-crunch-impacts
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Crowd Shouting/Speaking Ambience by StarNinjas | https://opengameart.org/content/crowd-shoutingspeaking-ambience 
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Adult male scream, injured and suffering sound effect by Mark Johnson |  https://gfxsounds.com/sound-effect/adult-male-scream-injured-and-suffering/
Sound promoted by https://gfxsounds.com/ 
Creative Commons: https://gfxsounds.com/licensing/standard-license/ 

Misc. Sound Effects: 
Point Bell by Brandon75689 | https://opengameart.org/content/point-bell
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Zippo click sound by dawith | https://opengameart.org/content/zippo-click-sound
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Metal slang sounds by bart | https://opengameart.org/content/metal-clang-sounds
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Door Open, Door Close Set by qubodup | https://opengameart.org/content/door-open-door-close-set
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Synthesized explosion by Iwan Gabovitch | https://opengameart.org/content/synthesized-explosion
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Missile sound by mikeask | https://opengameart.org/content/missile-sound
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

The Free Firearm Sound Library by Ben Jaszczak, et al. | https://opengameart.org/content/the-free-firearm-sound-library
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Helicopter SFX by WuxiaScrub | https://opengameart.org/content/helicopter-sfx 
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Wilhelm Scream downloaded at https://www.myinstants.com/instant/wilhelm-scream/ 

Missile Launch by Kibblesbob | https://soundbible.com/1794-Missle-Launch.html# 
Sound promoted by https://soundbible.com/ 
Creative Commons: Public Domain

Explosion by FlashTrauma | https://pixabay.com/sound-effects/explosion-6055/
Sound promoted by https://pixabay.com/
Creative Commons: https://pixabay.com/service/license/ 

Rifle Automatic Fire A Sound Effect | https://www.fesliyanstudios.com/royalty-free-sound-effects-download/automatic-gunfire-rifle-299 
Sound promoted by https://www.fesliyanstudios.com/ 
Creative Commons: https://www.fesliyanstudios.com/sound-effects-policy 

35 wooden cracks/hits/destructions by Independent.nu | https://opengameart.org/content/35-wooden-crackshitsdestructions
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

(defect) motor loop by qubodup | https://opengameart.org/content/defect-motor-loop
Sound promoted by https://opengameart.org/ 
Creative Commons: Public Domain

Mixkit Sounds:
Ambulance siren UK
Sound promoted by https://mixkit.co/free-sound-effects 
Creative Commons: https://mixkit.co/license/ 

Explosive impact from afar
Sound promoted by https://mixkit.co/free-sound-effects 
Creative Commons: https://mixkit.co/license/ 

Helicopter flying far away
Sound promoted by https://mixkit.co/free-sound-effects 
Creative Commons: https://mixkit.co/license/ 

Fire explosion
Sound promoted by https://mixkit.co/free-sound-effects 
Creative Commons: https://mixkit.co/license/ 

Fire swoosh burning
Sound promoted by https://mixkit.co/free-sound-effects 
Creative Commons: https://mixkit.co/license/ 

Rock golem walking
Sound promoted by https://mixkit.co/free-sound-effects 
Creative Commons: https://mixkit.co/license/ 

Emergency car arrival
Sound promoted by https://mixkit.co/free-sound-effects 
Creative Commons: https://mixkit.co/license/ 

Trailer screaming people annihilation 
Sound promoted by https://mixkit.co/free-sound-effects 
Creative Commons: https://mixkit.co/license/ 

Mixkit User terms: https://mixkit.co/terms/ 